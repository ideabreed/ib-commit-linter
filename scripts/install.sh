#!/bin/sh

repo_url="https://gitlab.com/ideabreed/ib-commit-linter"
script_file="https://gitlab.com/ideabreed/ib-commit-linter/-/raw/main/ib-commiter.sh"

function install {
    if [ ! -z $1 ]; then
        echo "Unsupported arg '$1'"
    fi
    repo_hooks_dir="${PWD}/.git/hooks"
    download_script=$(curl $script_file -o "${repo_hooks_dir}/commit-msg")

    chmod u+x "${repo_hooks_dir}/commit-msg"

    echo -e "\nInstalled ib-commiter as commit-msg hook in $repo_hooks_dir."
    echo "For usage see https://gitlab.com/ideabreed/ib-commit-linter#usage"
}

install $1