# IB-COMMITER | IB GIT COMMIT LINTER CLI

# ib-commiter

ib-commiter helps you follow the [Conventional Commits](https://www.conventionalcommits.org) conventional by installing a configurable `commit-msg` [Git Hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) into your Git projects.

### Prerequisites

To use ib-commiter, you must have [jq](https://stedolan.github.io/jq/download/) installed.

### Installing

```sh
curl -o- https://gitlab.com/ideabreed/ib-commit-linter/-/raw/main/scripts/install.sh | bash
```

### Uninstalling

Remove the `commit-msg` Git hook from your project:

```sh
rm <project-dir>/.git/hooks/commit-msg
```

## Usage <a name = "usage"></a>

Once installed, you must run `git init` in your Git projects to (re)initialize your repository. The hook will look for a configuration file in the following locations (in order):

1. Root DIR of Git project.

```json
{
  "enabled": true,
  "revert": true,
  "length": {
    "min": 1,
    "max": 52
  },
  "types": [
    "build",
    "ci",
    "docs",
    "feat",
    "fix",
    "perf",
    "refactor",
    "style",
    "test",
    "chore"
  ]
}
```

2. `GLOBAL_IB_COMMITER_CONFIG`: You can set a custom location for your `.ib-commiter.json` config by setting the `GLOBAL_IB_COMMITER_CONFIG` environment variable. Example: `GLOBAL_IB_COMMITER_CONFIG=$HOME/.ib-commiter/ib-commiter.json`.

**Note**: you can disable ib-commiter in your project by setting `enabled` to `false` in `.ib-commiter.json`.

### Future Implementation

1. Refactor config file / Remove the dependency of `jq` binary.
2. Optimize Regex
3. Add plugin support for pre-commit tasks for Django-docker based Projects.
4. Show Possible Problem Counts like other tools
