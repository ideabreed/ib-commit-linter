#!/bin/bash

repo_url="https://gitlab.com/ideabreed/ib-commit-linter"

function check_jq_exists {
if ! [ -x "$(command -v jq)" ]; then
  echo -e "\`commit-msg\` hook failed. Please install jq from https://stedolan.github.io/jq/download/"
  exit 1
fi
}

# check if the config file exists
# if it doesnt we dont need to run the hook
function verify_commiter_config {
  if [[ ! -f "$CONFIG" ]]; then
    echo -e "ib-commiter config file is missing. To set one see $repo_url#usage"
    exit 0
  fi
}

function set_config {
  project_config="$PWD/.ib-commiter.json"

  if [ -f "$project_config" ]; then
    CONFIG=$project_config
  elif [ -n "$GLOBAL_IB_COMMITER_CONFIG" ]; then
    CONFIG=$GLOBAL_IB_COMMITER_CONFIG
  fi
}

# set values from config file to variables
function set_config_values() {
  enabled=$(jq -r .enabled "$CONFIG")

  if [[ ! $enabled ]]; then
    exit 0
  fi

  revert=$(jq -r .revert "$CONFIG")
  types=($(jq -r '.types[]' "$CONFIG"))
  min_length=$(jq -r .length.min "$CONFIG")
  max_length=$(jq -r .length.max "$CONFIG")
}

# build the regex pattern based on the config file
function build_regex() {
  set_config_values

  regexp="^[.0-9]+$|"

  if $revert; then
      regexp="${regexp}^([Rr]evert|[Mm]erge):? )?.*$|^("
  fi

  for type in "${types[@]}"
  do
    regexp="${regexp}$type|"
  done

  regexp="${regexp%|})(\(.+\))?: "

  regexp="${regexp}.{$min_length,$max_length}$"
}


# Print out standard error message
function print_error() {
  commit_message=$1
  regular_expression=$2
  echo -e "\n[Invalid Commit Message]"
  echo -e "------------------------"
  echo -e "---------Config---------"
  echo -e "Valid types: ${types[@]}"
  echo -e "Length : $min_length - $max_length\n"
  echo -e "------------------------"
  echo -e "Error @ : $regular_expression"
  echo -e "Commit message: \"$commit_message\""
  echo -e "Length: $(echo $commit_message | wc -c)\n"
}

set_config
verify_commiter_config
check_jq_exists

# get the first line of the commit message
INPUT_FILE=$1
START_LINE=`head -n1 $INPUT_FILE`

build_regex

if [[ ! $START_LINE =~ $regexp ]]; then
  print_error START_LINE regexp
  exit 1
fi